<?php

namespace Nut;

/**
 * Permite establecer un directorio como almacen de funciones las cuales son
 * asociadas a expresiones regulares dentro de una pila, para ser ejecutadas al
 * comparar las espresiones regulares con una cadena especificada por el usuario.
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
final class Enrutador {

    /**
     * Nombre del directorio definido para almacenar las funciones.
     * @var string
     */
    public static $rutaReglas = "reglasURL";

    /**
     * Espacio de nombre predefinido agrupar las funciones almacenadas.
     * @var string
     */
    private static $nombreEspacio = NULL;

    /**
     * Arreglo cuyo indice es una expresion regular asociado a una funcion anonima
     * o almacenada.
     * @var string|callable
     */
    private static $patrones = [];

    /**
     * Definición privada del constructor para garantizar llamado estatico de la
     * clase.
     */
    private function __construct() {

    }

    /**
     * Método selector del espacio de nombre
     * @return string
     */
    private static function obtenerNombreDeEspacio() {
        if (is_null(self::$nombreEspacio)) {
            self::$nombreEspacio = str_replace('/', '\\', '\\Nut\\ReglaURL\\');
        }
        return self::$nombreEspacio;
    }

    /**
     * Método modificador del espacio de nombre
     * @param string $nombre
     */
    public static function modificarNombreDeEspacio($nombre) {
        self::$nombreEspacio = str_replace(['.', '\\\\'], '\\', "\\$nombre\\");
    }

    /**
     * Método que permite acceder a las funciones almacenadas en el directorio.
     * @param string $funcion
     * @return callable
     */
    private static function obtenerFuncion($funcion) {
        require_once str_replace('/', DIRECTORY_SEPARATOR, self::$rutaReglas . "/$funcion.php");
        return self::obtenerNombreDeEspacio() . $funcion;
    }

    /**
     * Permite asociar una expresion regular a una funcion anonima o almacenada.
     * @param string $patron
     * @param string|callable $funcion
     */
    public static function agregarRegla($patron, $funcion) {
        $noEsUnaFuncionAnonima = !($funcion instanceof \Closure);
        $noExisteFuncion = !is_callable($funcion);

        if ($noEsUnaFuncionAnonima && $noExisteFuncion) {
            $funcion = self::obtenerFuncion($funcion);
        }

        self::$patrones[$patron] = $funcion;
    }

    /**
     * Permite ejecutar una o varias funciones asociadas a una expresion regular
     * que coincida con la cadena definida.
     * @staticvar array $retornos
     * @param string $cadena
     */
    public static function ejecutarRegla($cadena) {
        static $retornos = [];
        foreach (self::$patrones as $patron => $funcion) {
            $coincidencias = [];
            $esCoincidente = preg_match_all("#$patron#i", $cadena, $coincidencias);
            if ($esCoincidente && is_null($retornos[$patron] = $funcion($coincidencias, $retornos))) {
                break;
            }
        }
    }

    /**
     * Permite ejecutar una funcion almacenada en el directorio de reglas.
     * @param string $funcion
     * @param array $coincidencias
     * @param array $retornos
     * @return callable
     */
    public static function ejecutarFuncion($funcion, $coincidencias, $retornos) {
        return self::obtenerFuncion($funcion)($coincidencias, $retornos);
    }

    /**
     * Permite reiniciar los atributos de la clase.
     */
    public static function reniciarPila() {
        self::$rutaReglas = "reglasURL";
        self::$nombreEspacio = NULL;
        self::$patrones = [];
    }

}
