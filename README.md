# Enrutador NUT

## Resumen
Este módulo esta destinado a la carga dinámica de funciones basado en la URL, define una clase estática que almacena funciones en una pila usando como identificador expresiones regulares que se usarán para comparar con una cadena definida por el usuario «URL» ejecutando en caso de coincidir «match» la función asociada en la pila.

## Descripción
### Atributos
**rutaReglas** permite especificar la ruta al directorio que almacena las funciones.
### Métodos
#### Basicos
**agregarRegla** crea o remplaza una asociación entre una expresión regular y una función.
**ejecutarRegla** ejecuta una función asociada a la expresión regular coincidente a la cadena indicada.
**reiniciarPila** borra todas las asociaciones existentes.
#### Configuración
**modificarNombreDeEspacio** modifica el espacio de nombre predeterminado «Nut\ReglaURL». Permite especificar una notación de punto para la definición de subpaquetes.

## Ejemplos
```php
<?php
/** Definición de Reglas **/
//Asocia cualquier expresión a la función [miFuncion]
Enrutador::agregarRegla('.*', 'miFuncion');
//Asocia cualquier expresión que inicie por [mi] a la función [miOtraFuncion]
Enrutador::agregarRegla('^mi', 'miOtraFuncion');
//Asocia cualquier expresión que termine por [url] a la función [miFuncionExtra]
Enrutador::agregarRegla('url$', 'miFuncionExtra');

/** Implementación de Reglas **/
//Ejecuta a miOtraFuncion y/o a miFuncion y/o a miFuncionExtra
Enrutador::ejecutarRegla('miURL', 'miOtraFuncion');
//Ejecuta a miOtraFuncion y/o a miFuncion y/o a miFuncionExtra
Enrutador::ejecutarRegla('mi/URL', 'miOtraFuncion');
//Ejecuta a miFuncion y/o a miFuncionExtra
Enrutador::ejecutarRegla('otraURL', 'miOtraFuncion');
```