<?php

use Nut\Enrutador;

/**
 * Descripcion de EnrutadorTest
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class EnrutadorTest extends \PHPUnit_Framework_TestCase {

  public static function setUpBeforeClass() {
    require '../src/Nut/Enrutador.php';

    function funcionQueNoDebeEjecutarse() {
      echo 'prueba ejecutada con error';
    }

    function funcionQueDebeEjecutarse() {
      echo 'prueba ejecutada con exito';
    }

  }

  public function setUp() {
    Enrutador::reniciarPila();
    /** Ajustamos la ruta de acceso a las reglas para coincidir con el directorio de pruebas unitarias */
    Enrutador::$rutaReglas = '../tests/' . Enrutador::$rutaReglas;
  }

  public static function tearDownAfterClass() {

  }

  public function testEjecutarReglaIgnorandoSensibilidadDeMayusculasYMinusculas() {
    Enrutador::agregarRegla('Hola Mundo', 'miReglaEnDirectorioIndicado');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito desde funcion en directorio editado');
  }

  public function testAgregarYEjecutarReglaUsandoFuncionPorDefinirComoParametro() {
    Enrutador::agregarRegla('.*', 'miReglaEnDirectorioIndicado');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito desde funcion en directorio editado');
  }

  public function testAgregarYEjecutarReglaUsandoFuncionYEspacioDeNombrePorDefinirComoParametro() {
    Enrutador::modificarNombreDeEspacio("Prueba.PHPUnit");
    Enrutador::agregarRegla('.*', 'miReclaConDiferenteEspacioDeNombre');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito desde funcion con espacio de nombre editado');
  }

  public function testAgregarYEjecutarReglaUsandoFuncionAnonimaComoParametro() {
    Enrutador::agregarRegla('.*', function () {
      echo 'prueba ejecutada con exito desde funcion anonima';
    });
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito desde funcion anonima');
  }

  public function testAgregarYEjecutarReglaUsandoFuncionExistenteComoParametro() {
    Enrutador::agregarRegla('.*', 'funcionQueDebeEjecutarse');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito');
  }

  public function testSobreEscribirReglas() {
    Enrutador::agregarRegla('.*', 'funcionQueNoDebeEjecutarse');
    Enrutador::agregarRegla('.*', 'funcionQueDebeEjecutarse');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito');
  }

  public function testDetenerEjecucionEnPrimerCoincidencia() {
    Enrutador::agregarRegla('.*', 'funcionQueNoDebeEjecutarse');
    Enrutador::agregarRegla('.*', 'funcionQueDebeEjecutarse');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString('prueba ejecutada con exito');
  }

  public function testEjecutarProximaReglaCoincidente() {
    Enrutador::agregarRegla('hola mundo', function() {
      echo 'prueba ejecutada con exito desde funcion anonima';
      return TRUE;
    });
    Enrutador::agregarRegla('.*', 'funcionQueDebeEjecutarse');
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString(
        'prueba ejecutada con exito desde funcion anonima' .
        'prueba ejecutada con exito'
    );
  }

  public function testObtenerRetornoDeEjecucionAUltimaReglaCoincidente() {
    $patronBuscado = 'hola mundo';
    $retornoEsperado = 'soy el retorno de una funcion previa';

    Enrutador::agregarRegla($patronBuscado, function() use($retornoEsperado) {
      return $retornoEsperado;
    });
    Enrutador::agregarRegla('.*', function($coincidencia, $retorno) use($patronBuscado) {
      unset($coincidencia);
      echo $retorno[$patronBuscado];
    });
    Enrutador::ejecutarRegla($patronBuscado);
    $this->expectOutputString($retornoEsperado);
  }

  public function testObtenerResultadoDeAplicarExpresionRegularAUltimaReglaCoincidente() {
    Enrutador::agregarRegla('(.).(.)a m(.)ndo', function($coincidencia) {
      echo json_encode($coincidencia);
    });
    Enrutador::ejecutarRegla("hola mundo");
    $this->expectOutputString(json_encode(
            [
                ["hola mundo"],
                ["h"],
                ["l"],
                ["u"]
            ]
    ));
  }

}
